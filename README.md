# Django Foldable Admin

### Github:

https://github.com/deniskrumko/django-foldable-admin

### Live example:

http://django-foldable-admin-live.bitballoon.com


# Django Admin Searchbar

### Github:

https://github.com/deniskrumko/django-admin-searchbar

### Live example:

http://django-admin-searchbar-live.bitballoon.com
