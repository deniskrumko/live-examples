// Javascript code for "django-admin-searchbar".
// Source: https://github.com/deniskrumko/django-admin-searchbar

// THIS IS NOT THE ORIGINAL FILE! IT'S JUST FOR DEMO!

$(document).ready(function() {
  // Variable for <input> HTML element
  var $searchbar = $('#django-admin-searchbar');

  // Change placeholder for searchbar when it's in focus
  $searchbar.focus(function() {
    $searchbar.attr('placeholder', 'Enter an object name to redirect');
  });

  // Change placeholder back when it loses focus (and remove text too)
  $searchbar.focusout(function() {
    $searchbar.attr('placeholder', 'Search...');
    $searchbar.val('');
  });

  // Autocomplete for searchbar (using "jQuery-autoComplete" plugin)
  // Source: https://github.com/Pixabay/jQuery-autoComplete
  $searchbar.autoComplete({
    minChars: 1,
    // Source is a processed search results from endpoint
    source: function(term, suggest) {
      term = term.toLowerCase();
      var suggestions = [];
      var search_results = [
        ['Microsoft Studios', 'Minecraft'],
        ['Microsoft Studios', 'Minesweeper'],
        ['Microsoft Studios', 'Halo'],
        ['FROM Software', 'Demon Souls'],
        ['FROM Software', 'Dark Souls'],
        ['FROM Software', 'Dark Souls II'],
        ['FROM Software', 'Dark Souls III'],
        ['FROM Software', 'Bloodborne'],
        ['Croteam', 'Serious Sam: The First Encounter'],
        ['Croteam', 'Serious Sam: The Second Encounter'],
        ['Croteam', 'Serious Sam 2'],
        ['Croteam', 'Serious Sam 3: BFE'],
        ['Croteam', 'The Talos Principle'],
        ['Activision', 'Call of Duty'],
        ['Activision', 'Call of Duty 2'],
        ['Activision', 'Call of Duty 3'],
        ['Activision', 'Call of Duty 4: Modern Warfare'],
        ['Activision', 'Call of Duty: World at War'],
        ['Activision', 'Call of Duty: Modern Warfare 2'],
        ['Activision', 'Call of Duty: Black Ops'],
        ['Activision', 'Call of Duty: Modern Warfare 3'],
        ['Activision', 'Call of Duty: Black Ops II'],
        ['Activision', 'Call of Duty: Ghosts'],
        ['Activision', 'Call of Duty: Advanced Warfare'],
        ['Activision', 'Call of Duty: Black Ops III'],
        ['Activision', 'Call of Duty: Infinite Warfare'],
        ['Activision', 'Call of Duty: WWII'],
        ['Nintendo', 'The Legend of Zelda'],
        ['Nintendo', 'Zelda II: The Adventure of Link'],
        ['Nintendo', 'The Legend of Zelda: A Link to the Past'],
        ['Nintendo', "The Legend of Zelda: Link's Awakening"],
        ['Nintendo', 'The Legend of Zelda: Ocarina of Time'],
        ['Nintendo', "The Legend of Zelda: Majora's Mask"],
        ['Nintendo', 'The Legend of Zelda: Oracle of Seasons'],
        ['Nintendo', 'The Legend of Zelda: Oracle of Ages'],
        ['Nintendo', 'The Legend of Zelda: The Wind Waker'],
        ['Nintendo', 'The Legend of Zelda: Four Swords Adventures'],
        ['Nintendo', 'The Legend of Zelda: Twilight Princess'],
        ['Nintendo', 'The Legend of Zelda: The Minish Cap'],
        ['Nintendo', 'The Legend of Zelda: Skyward Sword'],
        ['Nintendo', 'The Legend of Zelda: Phantom Hourglass'],
        ['Nintendo', 'The Legend of Zelda: Spirit Tracks'],
        ['Nintendo', 'The Legend of Zelda: A Link Between Worlds'],
        ['Nintendo', 'The Legend of Zelda: Tri Force Heroes'],
        ['Nintendo', 'The Legend of Zelda: Breath of the Wild'],
        ['Secret page', 'Secret'],
      ]
      for (i = 0; i < search_results.length; i++)
        if (~(search_results[i][1]).toLowerCase().indexOf(term)) {
          suggestions.push(search_results[i]);
        }
      suggest(suggestions);
    },
    // Render suggestions like "App name: Model name"
    renderItem: function(item, search) {
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
      return '<div class="autocomplete-suggestion" data-app="' + item[0] + '" data-val="' + search + '">' + '<strong>' + item[0] + ': </strong>' + item[1].replace(re, "<b>$1</b>") + '</div>';
    },
    onSelect: function(e, term, item) {
      // Redirect to selected page on "Enter" or mouse click
      if (item.data('app') == 'Secret page') {
        window.location.replace('fun.html');
      }
      $searchbar.val('Redirecting...');
      window.location.replace('results.html');
    }
  });
});
